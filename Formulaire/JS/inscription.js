$(function () {

    $.validator.addMethod("PWCHECK",
        function(value,element) {
            if(/^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&]){1,}).{8,}$/.test(value)){
                return true;
            }else{
                return false;
            };
        }
    );


    $("#inscription_form").validate(
        {
            rules:{
                nom_per:{
                    required: true,
                    minlength: 2,
                },
                prenom_per:{
                    required: true,
                    minlength: 2,
                },
                Email_per: {
                    required: true,
                    email: true,
                },
                password:{
                    required: true,
                    PWCHECK: true,
                },
                confirmpass_per:{
                    required: true,
                    equalTo: "#password"
                }
            },
            messages:{
                nom_per:{
                    required: "Veuillez saisir votre nom.",
                    minLength: "Votre nom doit être composé de 2 caractères au minimum"
                },
                prenom_per:{
                    required: "Veuillez saisir votre prénom.",
                    minLength: "Votre nom doit être composé de 2 caractères au minimum"
                },
                Email_per:{
                    required: "Votre adresse e-mail est indispensable à l'ouverture d'un compte",
                    email: "Veuillez saisir votre E-mail et doit avoir le format suivant : name@domain.com",
                },
                password:{
                    required: "Veuillez saisir votre mot de passe.",
                    PWCHECK: "Le mot de apsse doit comporter au minimum 8 caractères, dont une minuscule, une majuscule, un chiffre et un caractère spécial.",
                },
                confirmpass_per:{
                    equalTo: "Les mots de passe ne sont pas identiques",
                    required:"Veuillez saisir une deuxième fois votre mot de passe.",
                },
            }
        }
    )
});